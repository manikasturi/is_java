
public class Laceup implements Shoe, Category{
	
	private String materi ="";
	private String close_type ="";
	private int cushion =0;
	private String type_cat ="";
	private String type_lace ="";
	
	
	public Laceup(String leather, int mm, String close, String cat, String lace){
		
		materi = leather;
		cushion = mm;
		close_type = close;
		type_cat = cat;
		type_lace = lace;		
		
	}
	
	@Override
	public void material(String mat)
	{
		materi = mat;
	}
	
	@Override
	public void soleCushion(int mm){
		cushion = mm;
		
	}
	
	@Override
	public void closure(String closureType)
	{
		close_type = closureType;
	}
	
	@Override
	public void type_category(String type_category){
		
		type_cat = type_category;
		
	}
	
	public void type_Lacing(String lace){
		
		type_lace = lace;
		
	}

	
	// The 'get' methods to retrieve the values we set for the Laceup object.
	
		public String get_material(){
			return materi;
			
		}
		
		public int get_cushion(){
			return cushion;
			
		}
		
		public String get_close_type(){
			return close_type;
			
		}
		
		public String get_category(){
			return type_cat;
			
		}
		
		public String get_Lace(){
			return type_lace;
			
		}

		
}
