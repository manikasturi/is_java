
public class MyMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		BuckledShoe monk_strap = new BuckledShoe("Leather", 2, "Monk Strap", "Formal");
		System.out.println("Shoe_Type: " + monk_strap.get_close_type());
		System.out.println("Shoe Category: " + monk_strap.get_category());
		System.out.println("Cushion: " + monk_strap.get_cushion());
		System.out.println("Shoe Material: " + monk_strap.get_material());
		System.out.println(" ");
		
		Slipon loafer = new Slipon("Faux Leather ", 2, "Loafer", "Casual");
		System.out.println("Shoe_Type: " +loafer.get_close_type());
		System.out.println("Shoe Category: " + loafer.get_category());		
		System.out.println("Cushion: " + loafer.get_cushion());
		System.out.println("Shoe Material: " +loafer.get_material());
		System.out.println(" ");
		
		Laceup oxford = new Laceup("Leather", 2, "Oxford", "Formal", "Closed");
		System.out.println("Shoe_Type: " + oxford.get_close_type());
		System.out.println("Shoe Category: " + oxford.get_category());		
		System.out.println("Cushion: " + oxford.get_cushion());
		System.out.println("Shoe Material:" +oxford.get_material());
		System.out.println("Lace Type: " +oxford.get_Lace());
		System.out.println(" ");

		

	}

}
