
public class BuckledShoe implements Shoe, Category {
	
	private String materi ="";
	private String close_type ="";
	private int cushion =0;
	private String type_cat ="";
	
	
public BuckledShoe(String leather, int mm, String close, String cat){
		
		materi = leather;
		cushion = mm;
		close_type = close;
		type_cat = cat;		
		
	}
	
	@Override
	public void material(String mat)
	{
		materi = mat;
	}
	
	@Override
	public void soleCushion(int mm){
		cushion = mm;
		
	}
	
	@Override
	public void closure(String closureType)
	{
		close_type = closureType;
	}
	
	@Override
	public void type_category(String type_category){
		
		type_cat = type_category;
		
	}
	
	// The 'get' methods to retrieve the values we set for the bucked shoe object.
	
	public String get_material(){
		return materi;
		
	}
	
	public int get_cushion(){
		return cushion;
		
	}
	
	public String get_close_type(){
		return close_type;
		
	}
	
	public String get_category(){
		return type_cat;
		
	}

	
	
	

}
