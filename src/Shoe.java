// Basic interface for the component shoe
public interface Shoe {
	
	public void material(String mat);
	
	public void soleCushion(int mm);
	
	public void closure(String closureType);

}
